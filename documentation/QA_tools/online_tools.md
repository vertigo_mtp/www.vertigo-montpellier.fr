# QA

https://www.vertigo-montpellier.fr

## URL
* [http  + with www](http://www.vertigo-montpellier.fr)
* [http  + without www](http://vertigo-montpellier.fr)
* [https + with www](https://www.vertigo-montpellier.fr)
* [https + without www](https://vertigo-montpellier.fr)

### Files

* [/humans.txt](https://www.vertigo-montpellier.fr/humans.txt)
* [/robots.txt](https://www.vertigo-montpellier.fr/robots.txt)
* [/.well-known/security.txt](https://www.vertigo-montpellier.fr/.well-known/security.txt)

## QA - Online tools
`*` preconfigured tools


* Security 
    * [Hardenize](https://www.hardenize.com) (DNS, SMTP, web server)
    * [Mozilla Observatory](https://observatory.mozilla.org/analyze/www.vertigo-montpellier.fr) `*` (HTTP header, SSL, cookies, ...)
    * [Security Headers](https://securityheaders.io/?q=https://www.vertigo-montpellier.fr) `*` (HTTP header)
    * Content-Security-Policy (CSP)
        * [cspvalidator.org](https://cspvalidator.org/#url=https://www.vertigo-montpellier.fr) `*` 
        * [csp-evaluator.withgoogle.com](https://csp-evaluator.withgoogle.com/?csp=https://www.vertigo-montpellier.fr) `*` 
    * SSL
        * [ssllabs.com](https://www.ssllabs.com/ssltest/analyze?d=www.vertigo-montpellier.fr) `*` 
        * [tls.imirhil.fr](https://tls.imirhil.fr/https/www.vertigo-montpellier.fr) `*` 
    * DNS
        * [DNSViz](http://dnsviz.net/d/www.vertigo-montpellier.fr/dnssec/) `*` (DNSSEC)
        * [DNSSEC Analyzer (Verisign Labs)](https://dnssec-debugger.verisignlabs.com/www.vertigo-montpellier.fr) `*`   (DNSSEC)
        * [Zonemaster (iiS and AFNIC)](https://zonemaster.net/domain_check)
* W3C tools
    * [HTML validator](https://validator.w3.org/nu/?doc=https://www.vertigo-montpellier.fr&showsource=yes&showoutline=yes&showimagereport=yes) `*` 
    * [CSS validator](https://jigsaw.w3.org/css-validator/validator?uri=https://www.vertigo-montpellier.fr&profile=css3) `*` 
    * [i18n checker](https://validator.w3.org/i18n-checker/check?uri=https://www.vertigo-montpellier.fr) `*` 
    * [Link checker](https://validator.w3.org/checklink?uri=https://www.vertigo-montpellier.fr&hide_type=all&depth=&check=Check) `*` 
* Web accessibility
    * [Asqatasun](https://app.asqatasun.org)
* Web perf
    * [Yellowlab](http://yellowlab.tools)
    * [Webpagetest](https://www.webpagetest.org/)
    * [Test a single asset from 14 locations](https://tools.keycdn.com/performance?url=https://www.vertigo-montpellier.fr) `*`
* HTTP/2
    * [Http2.pro](https://http2.pro/check?url=https://www.vertigo-montpellier.fr) `*` (check server HTTP/2, ALPN, and Server-push support)
* Global tools (webperf, accessibility, security, ...)
    * [Dareboost](https://www.dareboost.com)  (free trial)
    * [Sonarwhal](https://sonarwhal.com/scanner/)

* Social networks
  * [Twitter card validator](https://cards-dev.twitter.com/validator)
* structured data (JSON-LD, rdf, schema.org, microformats.org, ...)
  * [Google structured data testing tool](https://search.google.com/structured-data/testing-tool#url=https://www.vertigo-montpellier.fr/)  `*` 
  * [Yandex structured data testing tool](https://webmaster.yandex.com/tools/microtest/)
  * [Structured Data Linter](http://linter.structured-data.org/?url=https://www.vertigo-montpellier.fr)  `*` 
  * [Microdata Parser](http://tools.seomoves.org/microdata/)
* Google image
  * [images used on the website](https://www.google.fr/search?tbm=isch&q=site:www.vertigo-montpellier.fr)  `*`  (site:www.vertigo-montpellier.fr)
  * [images used on the website but hosted on other domains](https://www.google.fr/search?tbm=isch&q=site:www.vertigo-montpellier.fr+-src:www.vertigo-montpellier.fr) `*`  (site:www.vertigo-montpellier.fr -src:www.vertigo-montpellier.fr)
  * [images hosted on the domain name](https://www.google.fr/search?tbm=isch&q=src:www.vertigo-montpellier.fr)  `*`    (src:www.vertigo-montpellier.fr)
  * [images hosted on the domain name and used by other domain names (hotlinks)](https://www.google.fr/search?tbm=isch&q=src:www.vertigo-montpellier.fr+-site:www.vertigo-montpellier.fr)  `*`   (src:www.vertigo-montpellier.fr -site:www.vertigo-montpellier.fr)



